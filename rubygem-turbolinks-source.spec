# Generated from turbolinks-source-5.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name turbolinks-source

Name: rubygem-%{gem_name}
Version: 5.0.0
Release: 1%{?dist}
Summary: Turbolinks JavaScript assets
Group: Development/Languages
License: MIT
URL: https://github.com/turbolinks/turbolinks-source-gem
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# We don't have js-turbolinks in Fedora yet and it would be quite demmanding
# to unbundle it :/ Anyway, I started discussion with upstream on this topic.
# https://github.com/turbolinks/turbolinks-rails/issues/11
Provides: bundled(turbolinks) = 5.0.0
BuildArch: noarch

%description
Turbolinks JavaScript assets.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




%check
pushd .%{gem_instdir}
# There are no tests upstream.
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md

%changelog
* Fri Jul 22 2016 Vít Ondruch <vondruch@redhat.com> - 5.0.0-1
- Initial package
